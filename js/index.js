/**
 * Created by tutyb on 4/8/2017.
 */
$(document).ready(function () {
    var socket = null;
    var warning = false;

    // $('#off-warning').prop('disabled', true);

    $('#status').css({
        'display': 'none'
    });

    $('.make-switch').css({
        'display': 'none'
    });

    //event setup
    $('#connect').click(connectToServer);

    $('#on').click(function () {
        if (reqLEDBrn(100)) {
            $('.bar').slider('value', 100);
            $('.project').find('.percent').val('100%');
        }
    });

    $('#off').click(function () {
        if (reqLEDBrn(0)) {
            $('.bar').slider('value', 0);
            $('.project').find('.percent').val('0%');
        }
    });

    $('#off-warning').click(function () {
        $('#warning').text('');
        // $(this).prop('disabled', true);
        socket.emit('offWarning', '');
    });

    $('#set-lowest').click(function () {
        if (socket === null) {
            console.log("Not connected to server!");
            return;
        }

        console.log($('#low').val());

        socket.emit('setLowest', {lowest: $('#low').val()});
    });

    $('#switch').on('switchChange.bootstrapSwitch', function (e, data) {
        changeAutoMode($(this).is(':checked'));
    });

    $('#switch').bootstrapSwitch();

    $('.project').each(function () {
        var $projectBar = $(this).find('.bar');
        var $projectPercent = $(this).find('.percent');
        var $projectRange = $(this).find('.ui-slider-range');
        $projectBar.slider({
            range: "min",
            animate: true,
            value: 1,
            min: 0,
            max: 100,
            step: 1,
            slide: function (event, ui) {
                $projectPercent.val(ui.value + "%");
                reqLEDBrn(ui.value);
                changeBarRange(ui.value);
            },
            change: function (event, ui) {
                var percent = ui.value;
                changeBarRange(percent);
            }
        });
    });

    function changeBarRange(percent) {
        var $projectPercent = $('.project').find('.percent');
        var $projectRange = $('.project').find('.ui-slider-range');
        var middle = middleColor(percent / 100);
        $projectPercent.css({
            'color': '#' + middle
        });
        $projectRange.css({
            'background': '#' + middle
        });
    }

    //socket handle
    function connectToServer() {
        if (socket !== null) {
            socket.disconnect();
            socket = null;
        }

        socket = io.connect('http://localhost:9695');
        socket.on('greetings', function (data) {
            console.log(data);
            socket.emit('clientGreeting', {clientName: "web client", message: "Hello there!"});
        });

        socket.on('dcnLC', function (data) {
            $('#status').text('LED controller is not connected');
            $('#status').css({
                'display': 'block'
            });
        });

        socket.on('LEDStatus', function (data) {
            var content = 'LED is ' + ((data.status) ? 'on' : 'off');

            $('#status').text(content);
            $('#switch').bootstrapSwitch('state', data.autoLED);

            $('#status').css({
                'display': 'block'
            });

            $('.make-switch').css({
                'display': 'block'
            });

            $('.bar').slider('value', data.percent);
            $('.project').find('.percent').val(data.percent + '%');
        });

        socket.on('alert', function (data) {
            $('#warning').text(data);
            $('#warning').css({
                'display': 'block'
            });
            // $('#off-warning').prop('disabled', false);
        });

        socket.on('offWarning', function (data) {
            $('#warning').text('');
        })
    };

    function reqLEDBrn(value) {
        if (socket === null) {
            console.log("Not connected to server!");
            return false;
        }
        socket.emit('reqLEDBrn', {status: true, percent: value});

        return true;
    }

    function changeAutoMode(autoLED) {
        socket.emit('autoLED', {'autoLED': autoLED});
    }

    function middleColor(ratio) {
        var color1 = '4cae4c';
        var color2 = 'D91E18';
        if (ratio < 0.3) {
            color1 = 'F4D03F';
        } else if (ratio < 0.7) {
            color2 = 'F4D03F';
        }
        var hex = function (x) {
            x = x.toString(16);
            return (x.length == 1) ? '0' + x : x;
        };

        var r = Math.ceil(parseInt(color1.substring(0, 2), 16) * ratio + parseInt(color2.substring(0, 2), 16) * (1 - ratio));
        var g = Math.ceil(parseInt(color1.substring(2, 4), 16) * ratio + parseInt(color2.substring(2, 4), 16) * (1 - ratio));
        var b = Math.ceil(parseInt(color1.substring(4, 6), 16) * ratio + parseInt(color2.substring(4, 6), 16) * (1 - ratio));

        return hex(r) + hex(g) + hex(b);
    }
});